package com.example.sayed.permissionhelper;

import android.Manifest;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import javax.xml.XMLConstants;

public class MainActivity extends AppCompatActivity implements IPermissionCallback {
    IPermissionCheck permissionCheck;
    private final String CAMERA_PERMISSION = Manifest.permission.CAMERA;
    private final String STORAGE_PERMISSION = Manifest.permission.READ_EXTERNAL_STORAGE;
    TextView textView;
    String s ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        permissionCheck = new PermissionCheck(this ,this);
        textView = (TextView) findViewById(R.id.text);
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionCheck.onPermissionRequestResult(requestCode , permissions , grantResults);
    }

    @Override
    public void onPermissionResult(List<String> grantedList, List<String> deniedList) {
        for(String S : grantedList){
            if(S.equals( CAMERA_PERMISSION)){
                s+="camera";
            }
            if(S.equals(STORAGE_PERMISSION)){
                s+="storage";
            }
        }
        textView.setText(s);
    }

    public void Click(View view) {
        permissionCheck.requestPermissions(CAMERA_PERMISSION , STORAGE_PERMISSION);
    }
}
