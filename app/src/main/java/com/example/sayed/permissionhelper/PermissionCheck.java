package com.example.sayed.permissionhelper;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sayed on 8/15/2017.
 */

public class PermissionCheck implements IPermissionCheck {
    private final int PERMISSION_REQUEST_CODE = 1;
    private WeakReference<Activity> activityWeakReference;
    private IPermissionCallback callback;
    private List<String> permissionList;

    public PermissionCheck(Fragment fragment, IPermissionCallback callback) {
        this(fragment.getActivity(), callback);
    }

    public PermissionCheck(Activity activity, IPermissionCallback callback) {
        activityWeakReference = new WeakReference<Activity>(activity);
        this.callback = callback;
        permissionList = setPermission();
    }

    boolean isMarshmallow() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }


    public List<String> setPermission() {
        List<String> per = new ArrayList<>();
        try {
            PackageManager pm = activityWeakReference.get().getApplicationContext().getPackageManager();
            PackageInfo pi = pm.getPackageInfo(activityWeakReference.get().getApplicationContext().getPackageName(), PackageManager.GET_PERMISSIONS);
            String permissionInfo[] = pi.requestedPermissions;
            for (String p : permissionInfo) {
                per.add(p);
            }
        } catch (Exception e) {

        }
        return per;
    }

    @Override
    public void requestPermissions(String... permissionsRequested) {
        if (isMarshmallow()) {
            for (String permString : permissionsRequested) {

                if (ContextCompat.checkSelfPermission(activityWeakReference.get(),
                        permString)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(activityWeakReference.get(),
                            new String[]{permString},
                            PERMISSION_REQUEST_CODE);
                } else {
                    List<String> list = new ArrayList<>();
                    list.add(permString);
                    callback.onPermissionResult(list, new ArrayList<String>());
                }
            }
        }
    }



    @Override
    public void onPermissionRequestResult(int requestCode, String[] permissions, int[] grantResults) {

        List<String> grantedList = new ArrayList<>();
        List<String> deniedList = new ArrayList<>();


        for (int i = 0; i < permissions.length; ++i) {
            String perm = permissions[i];
            if (grantResults[i] == PackageManager.PERMISSION_GRANTED)
                grantedList.add(perm);
            else
                deniedList.add(perm);

        }
        callback.onPermissionResult(grantedList, deniedList);

    }


}
