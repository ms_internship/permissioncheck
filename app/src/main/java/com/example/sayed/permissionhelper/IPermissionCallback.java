package com.example.sayed.permissionhelper;

import android.media.MediaRouter;
import android.net.Uri;

import java.util.List;

/**
 * Created by Sayed on 8/16/2017.
 */

public interface IPermissionCallback {

    void onPermissionResult(List<String> grantedList , List<String> deniedList);


}
