package com.example.sayed.permissionhelper;

import android.content.Context;

/**
 * Created by Sayed on 8/15/2017.
 */

public interface IPermissionCheck {



    void requestPermissions(String... permissionsRequested);

    void onPermissionRequestResult(int requestCode,
                                   String permissions[], int[] grantResults);

}
